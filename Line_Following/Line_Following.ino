//Line Following
// Let's Debug our Sensors
#include "Adafruit_PWMServoDriver.h"
Adafruit_PWMServoDriver pwm = Adafruit_PWMServoDriver(0x40);

// Hardware pins - defining pins at the top makes it easy to check wiring matches the code

// IR sensor data pins
#define IR_LEFT          A2
#define IR_MID           A1
#define IR_RIGHT         A0
// Motor speed & direction pins
#define RIGHT_FRONT_SPD  10
#define RIGHT_FRONT_DIR  11
#define RIGHT_REAR_SPD    8
#define RIGHT_REAR_DIR    9 
#define LEFT_FRONT_SPD   13
#define LEFT_FRONT_DIR   12
#define LEFT_REAR_SPD    15
#define LEFT_REAR_DIR    14
// Line-following tuning parameters
#define FORWARD_SPEED    50
#define TURN_SPEED       65
#define LINE_THRESHOLD  400

void setup()
{
  pwm.begin();
  pwm.setPWMFreq(60); // Analog servos run at ~60 Hz updates
  Clear_All_PWM();

  Serial.begin(115200); // baudrate

  pinMode(IR_RIGHT, INPUT);
  pinMode(IR_MID, INPUT);
  pinMode(IR_LEFT, INPUT);
}

void loop()
{
    // Read IR Sensor Values
    int Left_Value = analogRead(IR_LEFT);
    int Mid_Value = analogRead(IR_MID);
    int Right_Value = analogRead(IR_RIGHT);

    // Print for Serial Plotter
    Serial.print(Left_Value);   
    Serial.print(",");
    Serial.print(Mid_Value);
    Serial.print(",");
    Serial.println(Right_Value);
    
    // Check if line is under mid sensor --> go straight
    if(Mid_Value < LINE_THRESHOLD)
    {
      straight(FORWARD_SPEED);
    }
    // Check if line is under left sensor --> turn left
    else if(Left_Value < LINE_THRESHOLD)
    {
      turn_left(TURN_SPEED);
    }
    // Check if line is under right sensor --> turn right
    else if(Right_Value < LINE_THRESHOLD)   
    {
      turn_right(TURN_SPEED);  
    }
}

// Turn on all motors
void straight(int Speed)
{
  Set_Motors(Speed, Speed, Speed, Speed);
}

// Turn left - turn on right motors, turn off left motors
void turn_left(float Speed)
{
  Set_Motors(Speed, Speed, 0, 0);
}

// Turn right - turn off right motors, turn on left motors
void turn_right(float Speed)
{
  Set_Motors(0, 0, Speed, Speed);
}

// Set speed (0-255) of right front, right rear, left front, and left rear motors
void Set_Motors(int Right_Front_Speed, int Right_Rear_Speed, int Left_Front_Speed, int Left_Rear_Speed)
{
  Right_Front_Speed = Remap_Speed(Right_Front_Speed);
  Right_Rear_Speed = Remap_Speed(Right_Rear_Speed);
  Left_Front_Speed = Remap_Speed(Left_Front_Speed);
  Left_Rear_Speed = Remap_Speed(Left_Rear_Speed);

  // Right front wheel direction (forward)
  pwm.setPWM(RIGHT_FRONT_DIR, 0, 0);
  // Right front wheel speed
  pwm.setPWM(RIGHT_FRONT_SPD, 0, Right_Front_Speed); 

  // Right rear wheel direction (forward)
  pwm.setPWM(RIGHT_REAR_DIR, 0, 0);
  // Right rear wheel speed
  pwm.setPWM(RIGHT_REAR_SPD, 0, Right_Rear_Speed); 

  // Left front wheel direction (forward)
  pwm.setPWM(LEFT_FRONT_DIR, 0, 0);
  // Left front wheel speed
  pwm.setPWM(LEFT_FRONT_SPD, 0, Left_Front_Speed); 

  // Left rear wheel direction (forward)
  pwm.setPWM(LEFT_REAR_DIR, 0, 0);
  // Left rear wheel speed
  pwm.setPWM(LEFT_REAR_SPD, 0, Left_Rear_Speed); 
}

// Remaps integer between 0-255 to PWM width between 0-4095
int Remap_Speed(int Speed)
{
  return map(Speed, 0, 255, 0, 4095);
}

// Turn off all motors
void Clear_All_PWM()
{
  for (int i = 0; i < 16; i++)
  {
    pwm.setPWM(i, 0, 0);
  }
}
