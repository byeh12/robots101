//Ultrasound Intro
#include <Wire.h>

// Constants are usually in ALL CAPS
// to indicate the variable doesn't change value
const int ULTRASOUND_PIN = 12;

void setup()
{
  Serial.begin(9600);
  Serial.println("Ultrasonic sensor:");
}
 
void loop()
{
  // Trigger chirp
  
  pinMode(ULTRASOUND_PIN, OUTPUT);
  // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
  digitalWrite(ULTRASOUND_PIN, LOW);
  delayMicroseconds(2);
  // The sensor is triggered by a HIGH pulse of 10 or more microseconds:
  digitalWrite(ULTRASOUND_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(ULTRASOUND_PIN, LOW);
 
  // Receive echo

  pinMode(ULTRASOUND_PIN, INPUT);
  delayMicroseconds(50);
  float echo_time = pulseIn(ULTRASOUND_PIN, HIGH);
  float distance = echo_time / 58.00;

  // Print to serial

  Serial.print(distance);
  Serial.print("cm");
  Serial.println();

  delay(1000);
}
